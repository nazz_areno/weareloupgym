//
//  Model.swift
//  WeAreLoup Gym
//
//  Created by Nazzareno Cantalamessa on 20/02/2018.
//  Copyright © 2018 Nazzareno Cantalamessa. All rights reserved.
//

import Foundation

protocol GymProtocol {
    var name: String { get }
    var address: String { get }
}

struct Gym: GymProtocol {
    let name: String
    let address: String
}
