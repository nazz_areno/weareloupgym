//
//  GymApiService.swift
//  WeAreLoup Gym
//
//  Created by Nazzareno Cantalamessa on 20/02/2018.
//  Copyright © 2018 Nazzareno Cantalamessa. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol GymApiServiceProtocol {
    func fetchGyms(latitude: String, longitude: String, complete: @escaping ( _ success: Bool, _ gym: [Gym]?, _ error: Error? )->() )
}

class GymApiService: GymApiServiceProtocol {
    /**
     Get the Gym list
     
     - Parameters:
     - longitude: the type of View Controller
     - latitude: the current metric System used for height and weight
     
     - Returns: successful or fail response from the Service
     */
    func fetchGyms(latitude: String, longitude: String, complete: @escaping ( _ success: Bool, _ gym: [Gym]?, _ error: Error? )->() ) {
        let url = URL(string: "https://private-anon-eae29037db-fitlgdemo.apiary-mock.com/api/v1/gyms")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpBody = "{\n  \"latitude\": \(latitude),\n  \"longitude\": \(longitude)\n}".data(using: .utf8)
        
        print("latitude \(latitude)")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = response, let data = data {
                print(response)
                let json = JSON(data: data)
                var gyms: [Gym] = []
                for i in 0..<json.count {
                    if let name = json[i]["name"].string, let address = json[i]["address"].string {
                        let gym = Gym(name: name, address: address)
                        gyms.append(gym)
                    }
                }
                complete(true, gyms, nil)
            } else {
                complete(false, nil, error)
            }
        }
        
        task.resume()
    }
}
