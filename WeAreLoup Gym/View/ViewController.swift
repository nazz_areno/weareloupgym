//
//  ViewController.swift
//  WeAreLoup Gym
//
//  Created by Nazzareno Cantalamessa on 20/02/2018.
//  Copyright © 2018 Nazzareno Cantalamessa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    lazy var viewModel: GymViewModel = {
        return GymViewModel(apiService: GymApiService())
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initGymViewModelClosure()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Closures
    private func initGymViewModelClosure() {
        viewModel.reloadTableViewClosure = {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()
            }
        }
        viewModel.errorRequestClosure = { errorMessage in
            let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Refresh button action
    @IBAction func refressh(_ sender: Any) {
        viewModel.fetchGyms()
        activityIndicator.startAnimating()
    }
}

//MARK: TableView

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfGyms()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "gym_cell", for: indexPath) as? GymTableViewCell else {
            fatalError("Unknown Identifier")
        }
        
        //Fill cell data
        let cellViewModel = viewModel.getGymCell(at: indexPath)
        cell.name.text = cellViewModel.name
        cell.address.text = cellViewModel.address
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

//MARK: TableViewCell

class GymTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
}

