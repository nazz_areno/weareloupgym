//
//  WeAreLoup_GymTests.swift
//  WeAreLoup GymTests
//
//  Created by Nazzareno Cantalamessa on 20/02/2018.
//  Copyright © 2018 Nazzareno Cantalamessa. All rights reserved.
//
@testable import WeAreLoup_Gym

import Nimble
import Quick
import SwiftyJSON

class ViewModelSpec: QuickSpec {
    
    var sut: GymViewModel!
    var mockAPIService: MockGymApiService!
    
    override func spec() {
        describe("GymViewModel") {
            self.mockAPIService = MockGymApiService()
            self.sut = GymViewModel(apiService: self.mockAPIService)
            beforeEach {}
            context("when we populate the cell") {
                self.fetchGymFinished()
            }
            it("name and address in row 0 should have names and address character length greater than 0") {
                let indexPath = IndexPath(row: 0, section: 0)
                let vm = self.sut.getGymCell(at: indexPath)
                expect(vm.name.count) > 0
                expect(vm.address.count) > 0
            }
            it("name and address in row 1 should have names and address character length greater than 0") {
                let indexPath = IndexPath(row: 1, section: 0)
                let vm = self.sut.getGymCell(at: indexPath)
                expect(vm.name.count) > 0
                expect(vm.address.count) > 0
            }
            it("name and address in row 2 should have names and address character length greater than 0") {
                let indexPath = IndexPath(row: 2, section: 0)
                let vm = self.sut.getGymCell(at: indexPath)
                expect(vm.name.count) > 0
                expect(vm.address.count) > 0
            }
        }
    }
}

extension ViewModelSpec{
    func fetchGymFinished() {
        mockAPIService.gyms = MockGymJson().stubGyms()
        sut.fetchGyms()
        mockAPIService.fetchSuccess()
    }
}

class MockGymApiService: GymApiServiceProtocol {
    
    var gyms: [Gym] = []
    var completeClosure: ((Bool, [Gym]?, Error?) -> ())!
    
    func fetchGyms(complete: @escaping (Bool, [Gym]?, Error?) -> ()) {
        completeClosure = complete
    }
    
    func fetchSuccess() {
        completeClosure( true, gyms, nil )
    }
    
    func fetchFail(error: Error?) {
        completeClosure( false, nil, error )
    }
    
}

class MockGymJson {
    func stubGyms() -> [Gym] {
        let path = Bundle.main.path(forResource: "gyms", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let json = JSON(data: data)
        var gyms: [Gym] = []
        for i in 0..<json.count {
            if let name = json[i]["name"].string, let address = json[i]["address"].string {
                let gym = Gym(name: name, address: address)
                gyms.append(gym)
            }
        }
        return gyms
    }
}
