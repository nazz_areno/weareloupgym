//
//  ViewModel.swift
//  WeAreLoup Gym
//
//  Created by Nazzareno Cantalamessa on 20/02/2018.
//  Copyright © 2018 Nazzareno Cantalamessa. All rights reserved.
//

import Foundation

class GymViewModel {
    
    let apiService: GymApiServiceProtocol!
    var gyms: [Gym] = []
    var gymCellViewModelList: [GymCellViewModel] = []
    var isLoading = false
    let location = Location()
    
    //Closures
    var reloadTableViewClosure: (()->())?
    var errorRequestClosure: ((String)->())?
    
    public init (apiService: GymApiServiceProtocol = GymApiService()) {
        self.apiService = apiService
    }
    
    ///Start fetching the gyms
    func fetchGyms() {
        guard !isLoading else {
            return
        }
        isLoading = true
        
        let latitude = String(format: "%f", (location.locationManager.location?.coordinate.latitude)!)
        let longitude = String(format: "%f", (location.locationManager.location?.coordinate.longitude)!)
                
        apiService.fetchGyms(latitude: latitude, longitude: longitude) { (success, gyms, error) in
            if success {
                if let fetchedGyms = gyms {
                    self.gyms = fetchedGyms
                    self.setGyms(gyms: self.gyms)
                }
            } else {
                self.errorRequestClosure?("There has been an error during the request")
            }
            self.isLoading = false
        }
    }
    
    //Number of gyms
    func numberOfGyms() -> Int {
        return gyms.count
    }
    
    //Set GymCellViewModels
    func setGyms(gyms: [Gym]) {
        for gym in gyms {
            let gymCellViewModel = GymCellViewModel(name: gym.name, address: gym.address)
            gymCellViewModelList.append(gymCellViewModel)
        }
        self.reloadTableViewClosure?()
    }
    
    //Get info from a CellViewModel
    func getGymCell(at indexPath: IndexPath) -> GymCellViewModel{
        return gymCellViewModelList[indexPath.row]
    }
}

struct GymCellViewModel {
    let name: String
    let address: String
}
