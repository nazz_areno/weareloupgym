//
//  Location.swift
//  WeAreLoup Gym
//
//  Created by Nazzareno Cantalamessa on 20/02/2018.
//  Copyright © 2018 Nazzareno Cantalamessa. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class Location:NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    public override init() {
        super.init()
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest 
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            //print(location.coordinate)
        }
    }
}
